FROM atlassian/pipelines-awscli:1.16.18

RUN apk update && apk add curl
RUN curl -s https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.2.0/common.sh > /common.sh

COPY pipe.sh /

ENTRYPOINT ["/pipe.sh"]
