#!/usr/bin/env bats

set -e

setup() {
  # Create a unique application and deployment
  RANDOM_NUMBER=$RANDOM
  APPLICATION_NAME="bbci-code-deploy-test"
  DEPLOYMENT_GROUP="bbci-code-deploy-test-group"
}

teardown() {
  rm -rf ${BATS_TEST_DIRNAME}/tmp
}

@test "regression should run successfully with DEBUG enabled" {
    ## Create a revision
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *

    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e COMMAND="upload" \
      -e ZIP_FILE="${BATS_TEST_DIRNAME}/tmp/myapp.zip" \
      -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    # Run pipe
    run docker run \
      -e DEBUG='true' \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e WAIT="true" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      -e BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER} \
      bitbucketpipelines/aws-code-deploy
     
    [[ "${status}" == "0" ]]

    [[ "${output}" =~ "Deployment completed successfully" ]]
}


@test "should deploy a revision and wait for completion" {
    ## Create a revision
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *
    cd ${BATS_TEST_DIRNAME}

    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e COMMAND="upload" \
      -e ZIP_FILE="${BATS_TEST_DIRNAME}/tmp/myapp.zip" \
      -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    # Run pipe
    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e WAIT="true" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -e IGNORE_APPLICATION_STOP_FAILURES="true" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      -e BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER} \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]

    # Check out new app was deployed.
    run curl --silent http://13.236.115.90:8080

    [[ "${status}" == "0" ]]
    [[ "${output}" == "${RANDOM_NUMBER}" ]]
}

@test "should deploy a revision and skip waiting for completion" {
    ## Create a revision
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *

    # Run pipe
    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e WAIT="false" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      -e BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER} \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]
    [[ "${output}" =~ "Skip waiting for deployment to complete" ]]
}

@test "should ignore application stop errors when deploying a revision" {
  skip # flaky
    ## Create a revision
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app-broken-stop-script/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *

    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e COMMAND="upload" \
      -e ZIP_FILE="${BATS_TEST_DIRNAME}/tmp/myapp.zip" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      -e BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER} \
      bitbucketpipelines/aws-code-deploy


    # Run pipe deployment with broken stop script
    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -e WAIT="true" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]
    run curl --silent http://13.236.115.90:8080
    [[ "${output}" == "hello world" ]]


    # Verify second deployment fails.
    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -e WAIT="true" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "1" ]]

    # Verify we can now ignore app stop failures.
    run docker run \
      -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
      -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e DEPLOYMENT_GROUP="${DEPLOYMENT_GROUP}" \
      -e COMMAND="deploy" \
      -e IGNORE_APPLICATION_STOP_FAILURES="true" \
      -e FILE_EXISTS_BEHAVIOR="OVERWRITE" \
      -e WAIT="true" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]
}
